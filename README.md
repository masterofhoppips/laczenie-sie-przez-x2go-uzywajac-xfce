---

## Wykonana praca

### Połączenie z publicznym serwerem

1. Zainstaliśmy Archlinuxa na komputerach, skonfigurowaliśmy ssh, xfce, x2go.

2. Stworzyliśmy serwer z Archlinuxem na którym zainstalowaliśmy i skonfigurowaliśmy x2goserver oraz xfce.
	- IP serwera: 138.68.111.87
	- aby się połączyć konieczne jest dodanie klucza publicznego do kluczy zaufanych serwera
	
3. Połączyliśmy się z serwerem za pomocą x2goclient nie doświadczając żadnych problemów.

### Połączenie w sieci lokalnej pomiędzy dwoma komputerami

1. Połączyliśmy się lokalnie między dwoma komputerami z Archlinuxem. Problem czarnego ekranu wystąpił tylko, gdy łączyliśmy się do komputera z włacząnym xfce na tym samym koncie.

2. Problem można rozwiązać używając opcji Custom desktop z poleceniem "dbus-launch startxfce4", co prowadzi do stworzenia osobnego pulpitu na tym samym koncie i bezproblemowym połączeniem z nim.
	Z kolei użycie opcji "Connection to local desktop" spwoduje, że będziemy dzielić ten sam pulpit i każda akcja wykonywana przez jednego użytkownika będzie widoczna dla wszystkich.

---